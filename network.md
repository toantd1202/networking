## Networking overview 
### 1. Default network
Mặc định khi cài đặt xong, Docker sẽ tạo ra 3 card mạng mặc định là: - bridge - host - only.

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2000-36-42.png?raw=true)

Trong trường hợp, khi tạo container mà ta không chỉ định dùng network nào, thì docker sẽ dùng dải `bridge`.
+`none`: Các container thiết lập network này sẽ không được cấu hình mạng.
+ `bridge`: Docker sẽ tạo ra một switch ảo. Khi container được tạo ra, interface của container sẽ gắn vào switch ảo này và kết nối với interface của host.
+ `host`: Containers sẽ dùng mạng trực tiếp của máy host. Network configuration bên trong container đồng nhất với host.
### 2. bridge networks
Khi khởi động Docker, một `default bridge network` được tạo tự động và các container mới bắt đầu kết nối với nó trừ khi có quy định khác. Ngoài ra, người dùng thể tạo các bridge network tùy chỉnh do người dùng định nghĩa. User-defined bridge networks được đánh giá là vượt trội hơn  default bridge network.
#### Khác biệt giữa `user-defined bridges` và `default bridge`
+ user-defined bridges cung `DNS` tự động giữa các container.
tạo bridge có tên là my_bridge:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2001-16-55.png?raw=true)

kiểm tra my_bridge, có subnet: 172.18.0.0/16

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2001-17-40.png?raw=true)

lần lượt create 4 container:
```
$ docker run -dit --name alpine1 --network my_bridge alpine ash
$ docker run -dit --name alpine2 --network my_bridge alpine ash
$ docker run -dit --name alpine3 alpine ash
$ docker run -dit --name alpine4 alpine ash
```
![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2001-20-37.png?raw=true)

kiểm tra lần lượt các bridge(my_bridge, bridge)

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2001-22-00.png?raw=true)

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2001-23-26.png?raw=true)

kiểm tra kết nối giữa các container trong các bridge:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2001-25-51.png?raw=true)

attach vào container alpine1, thực hiện lần lượt ping tới alpine2 bằng tên và IPaddress và đều thành công. Trong khi đó, tương tự, attach vào container alpine3 thì chỉ có lần ping bằng IPaddress của alpine4 là thành công và thất bại khi ping bằng tên alpine4.
 
![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-30%2001-28-57.png?raw=true)

+ User-defined bridges cung cấp sự tách biệt tốt hơn.
  
Tất cả các container không chỉ định --network, đều được gắn vào default bridge network. Điều này dẫn tới một rủi ro, các container với các dịch vụ, chức năng không liên quan sau đó có thể giao tiếp.
Sử dụng user-defined network cung cấp một mạng có phạm vi trong đó chỉ các container được gắn vào mạng đó mới có thể giao tiếp.
+ Mỗi một user-defined network sẽ tạo ra một bridge có thể cấu hình. 
Nếu các container sử dụng default bridge network, người dùng có thể  cấu hình nó, nhưng tất cả các container đều sử dụng cùng một cài đặt.
User-defined bridge network được tạo và cấu hình bằng cách sử dụng `docker network create`. Nếu các nhóm ứng dụng khác nhau có các yêu cầu mạng khác nhau, ta có thể định cấu hình riêng từng bridge do người dùng xác định, khi tạo nó.
### 3. host networking
Nếu sử dụng `host` network mode cho một container, thì container’s network stack đó không bị cô lập với Docker host( container chia sẻ host’s networking namespace) và container không được cấp địa chỉ IP riêng. Chẳng hạn, nếu người dùng chạy một container liên kết với cổng 80 và sử dụng `host` networking, container’s application có sẵn trên cổng 80 trên địa chỉ IP của máy chủ lưu trữ.
  
Tại mode này dôcntainer không có địa chỉ IP riêng, nên port-mapping không có hiệu lực và tùy chọn -p, --publish, -P và --publish-all bị bỏ qua, Thay vào đó tạo ra một cảnh báo:
``` 
WARNING: Published ports are discarded when using host network mode
```

## practice 
### 1. network host
+ build image với Dockerfile:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-37-24.png?raw=true)

+ run container với network host.

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-53-49.png?raw=true)

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-56-34.png?raw=true)

lúc này, container chạy với IP address chung với docker host.

+ Kiểm tra webservice trên trình duyệt:
![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2023-08-56.png?raw=true)

+ Thử test với một máy khác chung vùng mạng với docker host:

![](https://github.com/toantd1202/photo/blob/master/host2.png?raw=true)

![](https://github.com/toantd1202/photo/blob/master/host1.png?raw=true)

--> nếu container được chạy với network host thì các máy tính cùng chung một mạng có thể dùng IPaddress của docker host chạy container để tương tác với container.

### 2. network bridge user-defined có expose port( Port expose k giống port app flask.)
+ build image với Dockerfile:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-02-54.png?raw=true)

+ create một network bridge có tên là my_net:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-07-02.png?raw=true)

+ run container với tên webservice

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-16-24.png?raw=true)

kiểm tra lại my_net, đã có container webservice bên trong mạng với điạ chỉ IP: 172.21.0.2.

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-17-36.png?raw=true)

+ kểm tra container đang chạy:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-20-57.png?raw=true)

+ xem kết quả trên trình duyệt:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-31-40.png?raw=true)

### 3. network bridge user-defined không có expose port
+ build một image mới tên là flask_app_2 với Dockerfile:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-37-24.png?raw=true)

+ run container với network my_net. Bên trong my_net đã có thêm container mới tên webservice1 có IPaddress là 172.21.0.3

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-40-18.png?raw=true)

+ kiểm tra container đang chạy:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-43-27.png?raw=true)

+ kiểm tra trên trình duyệt:

![](https://github.com/toantd1202/photo/blob/master/Screenshot%20from%202020-03-29%2022-45-14.png?raw=true)
